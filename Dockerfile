FROM node:16-slim

WORKDIR /usr/src/app

RUN apt-get update && apt-get -y install procps vim lsyncd gettext-base

RUN mkdir -p /etc/lsyncd
COPY lsyncd.conf.template /etc/lsyncd

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

VOLUME /usr/src/app/node_modules

ENTRYPOINT [ "/entrypoint.sh" ]

