#!/bin/bash

set -e

export PACKAGE_NAME=$(node -p "require('./package.json').name")

if [ -z "$PACKAGE_NAME" ]; then
	echo "ERROR: Cannot get packge name from package.json"
	exit 1
fi

export YARN_LINK_PATH=/usr/local/share/.config/yarn/link/${PACKAGE_NAME}
mkdir -p $YARN_LINK_PATH

cat /etc/lsyncd/lsyncd.conf.template | envsubst > /etc/lsyncd/lsyncd.conf

yarn install --frozen-lockfile
lsyncd /etc/lsyncd/lsyncd.conf
