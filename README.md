# Docker image linking the the NPM package in docker

Expose the package for linking with `yarn link` inside docker volume. Also install its NPM dependencies inside docker.

## Running

Run the docker image and bind two volumes

- library root directory to `/usr/src/app`
- YARN link volume to `/usr/local/share/.config/yarn/link`
